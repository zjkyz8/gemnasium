package composer

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/scanner/parser/testutil"
)

func TestComposer(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		tests := map[string]struct {
			fixtureDir     string
			expectationDir string
			opts           parser.Options
		}{
			"simple": {
				fixtureDir:     "simple",
				expectationDir: "simple",
				opts:           parser.Options{IncludeDev: true},
			},
			"big": {
				fixtureDir:     "big",
				expectationDir: "big",
				opts:           parser.Options{IncludeDev: true},
			},
			"big-no-dev": {
				fixtureDir:     "big",
				expectationDir: "big-no-dev",
				opts:           parser.Options{IncludeDev: false},
			},
		}
		for name, tt := range tests {
			tt := tt
			t.Run(name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tt.fixtureDir, "composer.lock")
				got, _, err := Parse(fixture, tt.opts)
				require.NoError(t, err)
				testutil.RequireExpectedPackages(t, tt.expectationDir, got)
			})
		}
	})
}
