// Package keystore provides high-level functions to update the Java keystore.
// It uses the cacert package of the analyzers common library to set the CA bundle.
package keystore

import (
	"bytes"
	"encoding/pem"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/cacert"
)

var keytoolExistingAliasErrorRegex = regexp.MustCompile(`(?mi)^keytool error: java.lang.Exception: Certificate not imported, alias (?P<certificate_name><[^>]+>) already exists$`)

// Update updates the CA cert bundle in the Java keystore, if defined
func Update(c *cli.Context) error {
	if !c.IsSet(cacert.FlagBundleContent) {
		return nil
	}

	bundle := []byte(c.String(cacert.FlagBundleContent))
	certs, err := parseCerts(bundle)
	if err != nil {
		return err
	}
	javaHome := os.Getenv("JAVA_HOME")
	return addCerts(javaHome, certs)
}

// addCerts adds each CA certificate to the java keystore
// to ensure that every certificate in the bundle is added.
// Existing aliases that cannot be imported are skipped.
// https://gitlab.com/gitlab-org/gitlab/-/issues/365484#note_1143871200
func addCerts(javaHome string, certs []io.Reader) error {
	for i, cert := range certs {
		alias := fmt.Sprintf("gl-dependency-scanning-custom-cert-%d", i)
		cmd := exec.Command(
			"keytool",
			"-importcert",
			"-alias", alias,
			"-trustcacerts",
			"-noprompt",
			"-storepass", "changeit",
			"-keystore", keystorePathFor(javaHome),
		)
		cmd.Dir = "/"
		cmd.Stdin = cert
		cmd.Env = os.Environ()
		output, err := cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), bytes.TrimSpace(output))

		if err != nil && isExistingAliasError(output) {
			log.Warnf("Certificate with alias %q previously loaded into keystore", alias)
			continue
		}
		if err != nil {
			return err
		}
	}
	return nil
}

func keystorePathFor(javaHome string) string {
	if isJava8(javaHome) {
		return path.Join(javaHome, "jre/lib/security/cacerts")
	}
	return path.Join(javaHome, "lib/security/cacerts")
}

func isJava8(javaHome string) bool {
	_, file := path.Split(javaHome)
	return strings.HasPrefix(file, "adoptopenjdk-8")
}

// parseCerts parses a certificate bundle into individual certificates
// that can be individually added to the java keystore. For more info
// see: https://gitlab.com/gitlab-org/gitlab/-/issues/365484#note_1143871200
func parseCerts(bundle []byte) ([]io.Reader, error) {
	if len(bundle) == 0 {
		log.Warnln("Empty additional certificate authority bundle provided")
		return nil, nil
	}

	var certs []io.Reader
	log.Debugln("Parsing individual certificates from additional certificate authority bundle ")
	for block, rest := pem.Decode(bundle); block != nil; block, rest = pem.Decode(rest) {
		log.Debugf("Found certificate of type %s in additional certificate authority bundle", block.Type)
		if block.Type != "CERTIFICATE" {
			log.Warnf("Skipping certificate: incorrect certificate of type %s found in additional certificate authority bundle", block.Type)
			continue
		}

		var out bytes.Buffer
		if err := pem.Encode(&out, block); err != nil {
			return nil, fmt.Errorf("parsing individual cert in additional certificate authority bundle: %v", err)
		}
		certs = append(certs, &out)
	}
	return certs, nil
}

// isExistingAliasError checks if the error printed by the keytool is caused by an existing certificate alias.
func isExistingAliasError(output []byte) bool {
	return keytoolExistingAliasErrorRegex.Match(output)
}
