package main

import (
	"crypto"
	"crypto/md5"
	"fmt"

	minio "github.com/minio/minio/pkg/madmin"
	log "github.com/sirupsen/logrus"
)

const tmpl = ""

func main() {
	m := md5.New()
	str := "test"
	m.Write([]byte(str))
	fmt.Printf("%x\n", m.Sum(nil))

	crypto.RegisterHash(crypto.MD5, md5.New)
	fmt.Println(crypto.MD5.Available())
	cm := crypto.MD5.New()
	cm.Write([]byte(str))
	//fmt.Printf("%x\n", cm.Sum(nil))
	fmt.Printf("%x\n", cm.Sum([]byte("test")))
	log.WithFields(log.Fields{
		"animal": "walrus",
	}).Info("A walrus appears")

	fmt.Println(minio.DriveStateOffline)
}
