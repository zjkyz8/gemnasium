package convert

import (
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

const (
	flagDepPathMode = "dependency-path-mode"
	flagSchemaModel = "schema-model"

	envVarDepPathMode = "DS_DEPENDENCY_PATH_MODE"
	envVarSchemaModel = "DS_SCHEMA_MODEL"

	defaultSchemaModel = uint(15)
)

// Flags returns the CLI flags that configure the converter
func Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagDepPathMode,
			EnvVars: []string{envVarDepPathMode},
			Usage:   "Dependencies for which the report gives one shortest path. Accepted values: all, none, affected",
			Value:   "affected",
		},
		&cli.UintFlag{
			Name:    flagSchemaModel,
			EnvVars: []string{envVarSchemaModel},
			Usage:   "Model of the Security Report JSON schema",
			Value:   defaultSchemaModel,
		},
	}
}

// Config configures how files returned by the scanner
// are converted to a Dependency Scanning report
type Config struct {
	AnalyzerDetails report.AnalyzerDetails
	ScannerDetails  report.ScannerDetails
	StartTime       *time.Time
}

// NewConverter initializes a new converter for a given config and CLI context
func NewConverter(c *cli.Context, cfg Config) *Converter {
	return &Converter{
		AnalyzerDetails: cfg.AnalyzerDetails,
		ScannerDetails:  cfg.ScannerDetails,
		StartTime:       cfg.StartTime,
		SchemaModel:     schemaModel(c),
		DepPathMode:     depPathMode(c),
	}
}

func schemaModel(c *cli.Context) uint {
	v := c.Uint(flagSchemaModel)
	if v != defaultSchemaModel {
		log.Warnf("schema model %d not supported", v)
	}
	log.Infof("using schema model %d", defaultSchemaModel)
	return defaultSchemaModel
}

func depPathMode(c *cli.Context) DepPathMode {
	switch c.String(flagDepPathMode) {
	case "all":
		return DepPathModeAll
	case "none":
		return DepPathModeNone
	case "affected":
		return DepPathModeAffected
	default:
		return DepPathModeAffected
	}
}
