package gem

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/vrange/cli"

func init() {
	cli.Register("gem", "gem/vrange.rb")
}
